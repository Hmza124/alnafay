package com.alnafay.app.adapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.alnafay.app.R;
import com.alnafay.app.interfaces.SendPointsToParent;
import com.alnafay.app.interfaces.onItemClick;
import com.alnafay.app.models.ProfileChilds;
import com.alnafay.app.models.UserDataModel;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterForChildList extends RecyclerView.Adapter<AdapterForChildList.Holder> {

    List<ProfileChilds> arrayList;
    Context context;
    LayoutInflater inflater;
    onItemClick onClick;

    public AdapterForChildList(List<ProfileChilds> arrayList, Context context, onItemClick onClick) {
        this.arrayList = arrayList;
        this.context = context;
        this.onClick = onClick;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @NotNull
    @Override
    public Holder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.child_view_item, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull AdapterForChildList.Holder holder, int position) {
        ProfileChilds childIds = arrayList.get(position);

        holder.childName.setText(childIds.getName()!=null ? childIds.getName() : "");

        if(childIds.getProfileImage()!=null && !childIds.getProfileImage().isEmpty())
            Picasso.get().load(childIds.getProfileImage()).placeholder(R.drawable.dummy_profile_image).into(holder.childProfileImage);

        if(childIds.getIsActive()!=null && childIds.getIsActive().equals("active")) {
            holder.bg_nameText.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.sea_green)));
            holder.childProfileImage.setBorderColor(context.getResources().getColor(R.color.sea_green));
        } else {
            holder.bg_nameText.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.red_orange)));
            holder.childProfileImage.setBorderColor(context.getResources().getColor(R.color.red_orange));
        }

        holder.addChildView.setOnClickListener(v -> onClick.onClick(childIds));
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static class Holder extends RecyclerView.ViewHolder {
        RelativeLayout addChildView;
        TextView childName;
        CircleImageView childProfileImage;
        LinearLayout bg_nameText;
        public Holder(@NonNull @NotNull View itemView) {
            super(itemView);

            addChildView = itemView.findViewById(R.id.addChildView);
            childName = itemView.findViewById(R.id.childName);
            childProfileImage = itemView.findViewById(R.id.childProfileImage);
            bg_nameText = itemView.findViewById(R.id.bg_nameText);
        }
    }
}
