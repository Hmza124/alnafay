package com.alnafay.app.util;

import com.alnafay.app.models.UserDataModel;

import java.util.ArrayList;

public class Constants {

    public static ArrayList<UserDataModel> userDataParentList = new ArrayList<>();
    public static ArrayList<UserDataModel> nodeParentList = new ArrayList<>();
    public static String fcmToken = "";
}
