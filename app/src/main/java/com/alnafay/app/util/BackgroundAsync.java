package com.alnafay.app.util;

import android.os.AsyncTask;

import com.alnafay.app.network.APIPresenter;

public class BackgroundAsync extends AsyncTask<Void, Integer, String> {
    int userID;
    APIPresenter apiPresenter;

    public void setData(int userID, APIPresenter apiPresenter) {
        this.userID = userID;
        this.apiPresenter = apiPresenter;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... voids) {
        apiPresenter.getChilds(userID);
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }
}
