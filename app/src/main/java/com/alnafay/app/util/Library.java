package com.alnafay.app.util;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.rejowan.cutetoast.CuteToast;

import java.util.UUID;

public class Library {

    private Context context;
    ProgressDialog progressDialog;

    public Library(Context context) {
        this.context = context;
    }

    public void showLoader() {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void hideLoader() {
        if(progressDialog!=null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static boolean passwordCount(CharSequence target) {
        return target.length() < 6;
    }

    public static boolean phoneCount(CharSequence target) {
        return target.length() < 11;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void copyValue(String value) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Refferal Code", value);
        clipboard.setPrimaryClip(clip);
        CuteToast.ct(context, "Copied", CuteToast.LENGTH_LONG, CuteToast.SUCCESS, true).show();
    }
}
