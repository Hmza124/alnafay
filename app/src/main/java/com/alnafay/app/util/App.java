package com.alnafay.app.util;

import android.app.Application;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        TypefaceUtil.overrideFont(getApplicationContext(), "MONOSPACE", "font/gilroy_medium.ttf");
    }
}
