package com.alnafay.app.popups;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.alnafay.app.R;
import com.alnafay.app.models.Profile;
import com.alnafay.app.screens.DashBoardScreen;
import com.alnafay.app.screens.GetPlanActiveScreen;

import org.jetbrains.annotations.NotNull;

import de.hdodenhof.circleimageview.CircleImageView;

public class PlanSubmitPopup extends DialogFragment {

    Profile profile;
    ImageView planGlide;
    Button finishBtn;
    AppCompatActivity activity;
    TextView message, title;
    String messageText = "";

    public static void showPopup(FragmentManager fragmentManager, Profile profile, AppCompatActivity activity, String messageText) {
        PlanSubmitPopup popup = new PlanSubmitPopup();
        popup.profile = profile;
        popup.activity = activity;
        popup.messageText = messageText;
        popup.setCancelable(false);
        popup.show(fragmentManager, "");
    }

    @NonNull
    @NotNull
    @Override
    public Dialog onCreateDialog(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.plan_submit_popup, null);
        intitView(view);
        builder.setView(view);

        return builder.create();
    }

    private void intitView(View view) {
        planGlide = view.findViewById(R.id.planGlide);
        finishBtn = view.findViewById(R.id.finishBtn);
        message = view.findViewById(R.id.message);
        title = view.findViewById(R.id.title);

        if(profile== null) {
            planGlide.setVisibility(View.GONE);
            title.setVisibility(View.GONE);
            finishBtn.setOnClickListener(v -> {
                dismiss();
            });
        } else {
            Glide.with(this)
                    .load(R.drawable.star)
                    .into(planGlide);

            finishBtn.setOnClickListener(v -> {
                DashBoardScreen.REQUEST_API = 1;
                activity.finish();
                dismiss();
            });
        }
        message.setText(messageText);
    }
}