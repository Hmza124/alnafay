package com.alnafay.app.popups;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.alnafay.app.R;
import com.alnafay.app.models.Profile;
import com.alnafay.app.util.Library;
import com.rejowan.cutetoast.CuteToast;

import org.jetbrains.annotations.NotNull;

public class ShareReferalCodePopup extends DialogFragment implements View.OnClickListener {

    String code = "";
    TextView codeTXT;
    Button copy, share;
    Library library;

    public static void showPopup(FragmentManager fragmentManager, String refferal) {
        ShareReferalCodePopup popup = new ShareReferalCodePopup();
        popup.code = refferal;
        popup.show(fragmentManager, "");
    }

    @NonNull
    @NotNull
    @Override
    public Dialog onCreateDialog(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.refferal_code_popup_layout, null);
        intitView(view);
        builder.setView(view);

        return builder.create();
    }

    private void intitView(View view) {
        codeTXT = view.findViewById(R.id.codeTXT);
        share  = view.findViewById(R.id.share );
        copy = view.findViewById(R.id.copy);

        library = new Library(requireActivity());
        codeTXT.setText(code);

        copy.setOnClickListener(this);
        share.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v == copy){

            library.copyValue(code);

        } else if(v == share) {
            shareCode();
        }
    }

    private void shareCode() {
        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        String shareBody = "Refferal code " + code;
        intent.setType("text/plain");
        intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        /*Fire!*/
        startActivity(Intent.createChooser(intent, "Share refferal code"));
    }
}