package com.alnafay.app.popups;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.bumptech.glide.Glide;
import com.alnafay.app.R;
import com.alnafay.app.models.Profile;
import com.alnafay.app.network.APIPresenter;
import com.alnafay.app.network.APIResponse;
import com.alnafay.app.network.refreshResponse;
import com.alnafay.app.screens.DashBoardScreen;
import com.alnafay.app.screens.LoginScreen;
import com.alnafay.app.util.Library;
import com.rejowan.cutetoast.CuteToast;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;

public class ProfilePopup extends DialogFragment implements APIResponse, View.OnClickListener {

    CircleImageView profileImage;
    EditText typeEditText, phoneEditText, emailEditText;
    Profile profile;
    RelativeLayout profileImgRV;
    Button logoutBtn, uploadBtn;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Activity activity;
    ProgressBar progressBar;
    APIPresenter apiPresenter;
    private refreshResponse refreshResponse;
    Library library;
    TextView userName;

    public static void showPopup(FragmentManager fragmentManager, Profile profile, Activity activity, refreshResponse refreshResponse) {
        ProfilePopup popup = new ProfilePopup();
        popup.profile = profile;
        popup.activity = activity;
        popup.refreshResponse = refreshResponse;
        popup.show(fragmentManager, "");
    }

    @NonNull
    @NotNull
    @Override
    public Dialog onCreateDialog(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.profile_popup_layout, null);
        intitView(view);
        builder.setView(view);

        return builder.create();
    }

    private void intitView(View view) {
        profileImage = view.findViewById(R.id.profileImage);
        typeEditText = view.findViewById(R.id.typeEditText);
        phoneEditText = view.findViewById(R.id.phoneEditText);
        emailEditText = view.findViewById(R.id.emailEditText);
        profileImgRV = view.findViewById(R.id.profileImgRV);
        logoutBtn = view.findViewById(R.id.logoutBtn);
        progressBar = view.findViewById(R.id.progressBar);
        userName = view.findViewById(R.id.userName);
        uploadBtn = view.findViewById(R.id.uploadBtn);

        apiPresenter = new APIPresenter(activity, this);
        library = new Library(activity);

        sharedPreferences = requireActivity().getSharedPreferences("status_pref", activity.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if(!profile.getIsActive().equals("active")) {
            typeEditText.setText("Buy account");
        } else {
            typeEditText.setText(profile.getAccountType()!=null ? profile.getAccountType() : "");
        }

        if(profile.getProfileImage()!=null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Picasso.get().load(profile.getProfileImage()).placeholder(R.drawable.dummy_profile_image).into(profileImage);
                }
            });

        }
        userName.setText(profile.getName()!=null ? profile.getName() : "");

        phoneEditText.setText(profile.getPhoneNumber()!=null ? profile.getPhoneNumber() : "");
        emailEditText.setText(profile.getEmail()!=null ? profile.getEmail() : "");

        profileImgRV.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent,
                    "Select Picture"), 1000);
        });

        logoutBtn.setOnClickListener(v -> {

            String email = sharedPreferences.getString("email", "");
            String password = sharedPreferences.getString("password", "");

            apiPresenter.loginUser(email, password, "");
            library.showLoader();
        });

        uploadBtn.setOnClickListener(this);
    }
    Bitmap bitmap;
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == 1000) {
                try {
                    Uri selectedImageUri = data.getData();
                    bitmap = handleSamplingAndRotationBitmap(activity, MediaStore.Images.Media.getBitmap(activity.getContentResolver(), selectedImageUri), selectedImageUri);
                    profileImage.setImageBitmap(bitmap);
                    uploadBtn.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public Bitmap handleSamplingAndRotationBitmap(Context context, Bitmap bitmap, Uri selectedImage) throws IOException {
        ExifInterface ei;

        if (Build.VERSION.SDK_INT >= 24)
            ei = new ExifInterface(context.getContentResolver().openInputStream(selectedImage));
        else ei = new ExifInterface(selectedImage.toString());

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        Bitmap rotatedBitmap;
        switch (orientation) {

            case ExifInterface.ORIENTATION_ROTATE_90:
                rotatedBitmap = rotateImage(bitmap, 90);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                rotatedBitmap = rotateImage(bitmap, 180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                rotatedBitmap = rotateImage(bitmap, 270);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
            default:
                rotatedBitmap = bitmap;
        }

        return rotatedBitmap;
    }

    public Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return bitmap = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    @Override
    public void onClick(View v) {
        if(v == uploadBtn) {
            Base64ConversionSync async = new Base64ConversionSync();
            async.execute();
        }
    }

    public String convertBitmapToByteArray(Bitmap bitmap) {
        byte[] byteArray;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            int mbSize = (bitmap.getByteCount() / 1024) / 1024;

            if (mbSize <= 10) {

                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream);

            } else if (mbSize <= 15) {

                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
            } else if (mbSize <= 25) {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 60, byteArrayOutputStream);
            } else {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
            }

            byteArray = byteArrayOutputStream.toByteArray();
            byteArrayOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        return Base64.encodeToString(byteArray, Base64.NO_WRAP);
    }

    private void upload(String ansValue) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", profile.getId());
            jsonObject.put("image_name", String.valueOf(System.currentTimeMillis()) + ".jpg");
            jsonObject.put("image_file", ansValue);

            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),(jsonObject).toString());
            apiPresenter.updateImage(body);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccess(String response, String type) {
        library.hideLoader();
        try{
            if(response!=null && type.equals("updateImage")) {
                boolean jsonObject = new JSONObject(response).getBoolean("status");
                if(jsonObject) {
                    DashBoardScreen.REQUEST_API = 1;
                    CuteToast.ct(activity, "Profile image updated.", CuteToast.LENGTH_LONG, CuteToast.SUCCESS, true).show();
                    dismiss();
                    refreshResponse.onRefresh();
                }
            } else if(response!=null && type.equals("login")) {
                JSONObject object = new JSONObject(response);
                boolean status = object.getBoolean("status");
                if(status) {
                    logout();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            uploadBtn.setVisibility(View.GONE);
            Picasso.get().load(profile.getProfileImage()).placeholder(R.drawable.dummy_profile_image).into(profileImage);
            CuteToast.ct(activity, "Please try again.", CuteToast.LENGTH_LONG, CuteToast.ERROR, true).show();
        }
    }

    private void logout() {
        editor.putInt("userID", -1);
        editor.apply();
        Intent intent = new Intent(activity, LoginScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        dismiss();
        activity.finish();
    }

    @Override
    public void onFailure(String message) {
        library.hideLoader();
        uploadBtn.setVisibility(View.GONE);
        Picasso.get().load(profile.getProfileImage()).placeholder(R.drawable.dummy_profile_image).into(profileImage);
        CuteToast.ct(activity, "Please try again.", CuteToast.LENGTH_LONG, CuteToast.ERROR, true).show();
    }

    private class Base64ConversionSync extends AsyncTask<Void, Integer, String> {

        protected void onPreExecute() {
            super.onPreExecute();
            library.showLoader();
        }

        protected String doInBackground(Void...arg0) {
            String base64 = convertBitmapToByteArray(bitmap);
            upload(base64);
            return "You are at PostExecute";
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            CuteToast.ct(activity, "It takes a while. Please wait...", CuteToast.LENGTH_LONG, CuteToast.WARN, true).show();
        }
    }
}
