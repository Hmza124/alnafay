package com.alnafay.app.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.alnafay.app.R;
import com.alnafay.app.adapters.AdapterForChildList;
import com.alnafay.app.interfaces.onItemClick;
import com.alnafay.app.models.ProfileChilds;
import com.alnafay.app.models.ProfileData;
import com.alnafay.app.network.APIPresenter;
import com.alnafay.app.network.APIResponse;
import com.alnafay.app.screens.DashBoardScreen;
import com.alnafay.app.util.Library;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChildDetailFragment extends Fragment implements onItemClick, APIResponse {

    TextView memberName, status, phoneNumber, norecordfound, emailTEXT, accountType;
    RecyclerView recylerView;
    Library library;
    ImageView crossImage;
    private ProfileChilds userDataChildList;
    CircleImageView profileImage;
    LinearLayout deleteLL;
    ProgressBar progressBar;
    APIPresenter apiPresenter;
    boolean isDirect = false;

    public ChildDetailFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        userDataChildList = (ProfileChilds) Objects.requireNonNull(args).getSerializable("childData");
        isDirect = Objects.requireNonNull(args).getBoolean("isDirect");
        apiPresenter = new APIPresenter(requireActivity(), this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.child_detail_fragment, container, false);
        library = new Library(getContext());
        recylerView = view.findViewById(R.id.recylerView);
        memberName = view.findViewById(R.id.memberName);
        status = view.findViewById(R.id.status);
        accountType = view.findViewById(R.id.accountType);
        crossImage = view.findViewById(R.id.crossImage);
        norecordfound = view.findViewById(R.id.norecordfound);
        profileImage = view.findViewById(R.id.profileImage);
        deleteLL = view.findViewById(R.id.deleteLL);
        progressBar = view.findViewById(R.id.progressBar);
        emailTEXT = view.findViewById(R.id.emailTEXT);

        if(userDataChildList!=null){
            memberName.setText(userDataChildList.getName()!=null ? userDataChildList.getName() : "");
            status.setText((userDataChildList.getIsActive()!=null && userDataChildList.getIsActive().equals("active")) ? "Verified" : "Inactive");
            accountType.setText(userDataChildList.getAccountType()!=null ? userDataChildList.getAccountType() : "");
            emailTEXT.setText(userDataChildList.getEmail()!=null ? userDataChildList.getEmail() : "");

            if(userDataChildList.getProfileImage()!=null && !userDataChildList.getProfileImage().isEmpty())
                Glide.with(this).load(userDataChildList.getProfileImage()).placeholder(R.drawable.dummy_profile_image).dontAnimate().into(profileImage);

            if(userDataChildList.getIsActive()!=null && userDataChildList.getIsActive().equals("active")) {

                apiPresenter.getChilds(userDataChildList.getId());
                progressBar.setVisibility(View.VISIBLE);
                recylerView.setLayoutManager(new GridLayoutManager(requireActivity(), 3));
                deleteLL.setVisibility(View.GONE);
            } else if(isDirect) {
                recylerView.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                int unicode = 0x26D4;
                accountType.setText(new String(Character.toChars(unicode)));
            } else {
                int unicode = 0x26D4;
                accountType.setText(new String(Character.toChars(unicode)));
                progressBar.setVisibility(View.GONE);
                norecordfound.setVisibility(View.VISIBLE);
                recylerView.setVisibility(View.GONE);
            }
        }

        crossImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        return view;
    }

    @Override
    public void onClick(ProfileChilds value) {
        ((DashBoardScreen)getActivity()).opentFragment(value, false);
    }

    @Override
    public void onSuccess(String response, String type) {
        try {
            progressBar.setVisibility(View.GONE);
            if(response!=null && !response.isEmpty() && type.equals("child")) {
                JSONObject jsonObject = new JSONObject(response);
                JSONObject childs = new JSONObject(jsonObject.get("payload").toString());

                Type listType = new TypeToken<ArrayList<ProfileChilds>>() {}.getType();
                ArrayList<ProfileChilds> getChildDataList = new Gson().fromJson(childs.get("childs").toString(), listType);
                if(getChildDataList!=null && getChildDataList.size() > 0) {
                    recylerView.setVisibility(View.VISIBLE);
                    norecordfound.setVisibility(View.GONE);
                    recylerView.setAdapter(new AdapterForChildList(getChildDataList, requireActivity(), this));
                } else {
                    norecordfound.setVisibility(View.VISIBLE);
                    recylerView.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onFailure(String message) {
        progressBar.setVisibility(View.GONE);
    }
}