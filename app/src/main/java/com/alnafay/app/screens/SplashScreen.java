package com.alnafay.app.screens;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.alnafay.app.R;

public class SplashScreen extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    ImageView logoImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        logoImage = findViewById(R.id.logoImage);

        sharedPreferences = getSharedPreferences("status_pref", MODE_PRIVATE);
        int userID = sharedPreferences.getInt("userID", -1);

        new Handler().postDelayed(() -> {

            Intent intent;
            if(userID!=-1) {
                intent = new Intent(SplashScreen.this, DashBoardScreen.class);
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(SplashScreen.this,
                        (View)logoImage, "logo");
                startActivity(intent, options.toBundle());
            } else {
                intent = new Intent(SplashScreen.this, LoginScreen.class);
                startActivity(intent);
            }
            finish();
        }, 3000);
    }
}