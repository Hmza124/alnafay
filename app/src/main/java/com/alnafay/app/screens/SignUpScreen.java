package com.alnafay.app.screens;

import androidx.appcompat.app.AppCompatActivity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.alnafay.app.R;
import com.alnafay.app.network.APIPresenter;
import com.alnafay.app.network.APIResponse;
import com.alnafay.app.util.Library;
import com.rejowan.cutetoast.CuteToast;

public class SignUpScreen extends AppCompatActivity implements View.OnClickListener, APIResponse {

    private Button signUpBtn;
    private EditText referalEditText, phoneEditText, nameEditText, passwordEditText, passwordConfirmEditText;
    private LinearLayout dim_layer;
    private Library library;
    private boolean isDefault = false;
    TextView emailEditText;
    APIPresenter apiPresenter;
    ProgressBar progress;
    GoogleSignInClient mGoogleSignInClient;
    SignInButton signInButton;
    private String imageUrl = "", imageName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_screen);

        signUpBtn = findViewById(R.id.sign_up_btn);
        referalEditText = findViewById(R.id.referalEditText);
        emailEditText = findViewById(R.id.emailEditText);
        phoneEditText = findViewById(R.id.phoneEditText);
        nameEditText = findViewById(R.id.nameEditText);
        passwordEditText = findViewById(R.id.passwordEditText);
        passwordConfirmEditText = findViewById(R.id.passwordConfirmEditText);
        progress = findViewById(R.id.progress);
        dim_layer = findViewById(R.id.dim_layer);

        library = new Library(this);
        apiPresenter = new APIPresenter(this, this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);

        signInButton = findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_ICON_ONLY);

        signUpBtn.setOnClickListener(this);
        signInButton.setOnClickListener(this);
        emailEditText.setOnClickListener(this);
    }

    private void registerUser() {
        dim_layer.setVisibility(View.VISIBLE);
        progress.setVisibility(View.VISIBLE);

        String email = emailEditText.getText().toString().trim();
        String password = passwordEditText.getText().toString().trim();
        String confirm_password = passwordConfirmEditText.getText().toString().trim();
        String phone = phoneEditText.getText().toString().trim();
        String username = nameEditText.getText().toString().trim();
        String refID = referalEditText.getText().toString().trim();

        apiPresenter.registerUser(username, email, password, confirm_password, username, phone, refID, imageUrl, imageName);
    }

    private boolean validation() {
        Library.hideKeyboard(this);
        if(emailEditText.getText().toString().isEmpty()) {
            emailEditText.setHintTextColor(Color.RED);
            emailEditText.setHint("Email Required");
            return false;
        }
        else if(phoneEditText.getText().toString().isEmpty()) {
            phoneEditText.setHintTextColor(Color.RED);
            phoneEditText.setHint("Phone Required");
            return false;
        }
        else if(nameEditText.getText().toString().isEmpty()) {
            nameEditText.setHintTextColor(Color.RED);
            nameEditText.setHint("Name Required");
            return false;
        }
        else if(passwordEditText.getText().toString().isEmpty()) {
            passwordEditText.setHintTextColor(Color.RED);
            passwordEditText.setHint("Password Required");
            return false;
        }
        else if(passwordConfirmEditText.getText().toString().isEmpty()) {
            passwordConfirmEditText.setHintTextColor(Color.RED);
            passwordConfirmEditText.setHint("Confirm Password Required");
            return false;
        }
        else if(!Library.isValidEmail(emailEditText.getText().toString())) {
            emailEditText.setText("");
            emailEditText.setHintTextColor(Color.RED);
            emailEditText.setHint("Email address is invalid");
            return false;
        }
        else if(Library.phoneCount(phoneEditText.getText().toString().trim())) {
            phoneEditText.setText("");
            phoneEditText.setHintTextColor(Color.RED);
            phoneEditText.setHint("Phone number not valid (00000000000)");
            return false;
        }
        else if(Library.passwordCount(passwordEditText.getText().toString().trim())) {
            passwordEditText.setText("");
            passwordEditText.setHintTextColor(Color.RED);
            passwordEditText.setHint("The password must be at least 6 characters.");
            return false;
        }
        else if(!passwordConfirmEditText.getText().toString().trim().equals(passwordEditText.getText().toString().trim())) {
            passwordConfirmEditText.setText("");
            passwordConfirmEditText.setHintTextColor(Color.RED);
            passwordConfirmEditText.setHint("Password not match");
            return false;
        }
        else if(referalEditText.getText().toString().isEmpty()) {
            if(!isDefault)
                showDialogRef();
            return isDefault;
        }
        return true;
    }

    private void showDialogRef() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Are you sure ?");
        alert.setMessage("You didn't put Refferal ID. You want to use default Refferal ID");
        alert.setNegativeButton("No I have Refferal ID", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                isDefault = false;
            }
        });

        alert.setPositiveButton("Use Default", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                isDefault = true;
            }
        });
        alert.show();
    }
    
    @Override
    public void onClick(View v) {
        if(v == signUpBtn) {
            if(validation()) {
                registerUser();
            }
        } else if(v == signInButton) {
            signIn();
        } else if(v == emailEditText) {
            if(emailEditText.getText().toString().isEmpty())
                CuteToast.ct(this, "Please click on google button.", CuteToast.LENGTH_LONG, CuteToast.WARN, true).show();
        }
    }

    private void signIn() {
        mGoogleSignInClient.signOut();
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, 100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> task) {
        try {
            GoogleSignInAccount account = task.getResult(ApiException.class);
            nameEditText.setText(account.getDisplayName()!=null ? account.getDisplayName() : "");
            emailEditText.setText((account.getEmail()!=null) ? account.getEmail() : "Error while getting email address");
            imageUrl = account.getPhotoUrl()!=null ? account.getPhotoUrl().toString() : "";
            if(!imageUrl.isEmpty())
                imageName = System.currentTimeMillis() + ".jpg";
        } catch (ApiException e) {
            CuteToast.ct(this, "Please check your google account settings. Something went wrong", CuteToast.LENGTH_LONG, CuteToast.ERROR, true).show();
        }

    }

    @Override
    public void onSuccess(String response, String type) {
        if(response!=null && !response.isEmpty()) {
            CuteToast.ct(this, "You have registered successfully", CuteToast.LENGTH_LONG, CuteToast.SUCCESS, true).show();
            Intent intent = new Intent(SignUpScreen.this, LoginScreen.class);
            startActivity(intent);
            finish();
        } else {
            dim_layer.setVisibility(View.GONE);
            progress.setVisibility(View.GONE);
            CuteToast.ct(this, "Something went wrong", CuteToast.LENGTH_LONG, CuteToast.ERROR, true).show();
        }
    }

    @Override
    public void onFailure(String message) {
        if(message!=null && !message.isEmpty()) {
            CuteToast.ct(this, message, CuteToast.LENGTH_LONG, CuteToast.ERROR, true).show();
        } else {
            CuteToast.ct(this, "Something went wrong", CuteToast.LENGTH_LONG, CuteToast.ERROR, true).show();
        }
        dim_layer.setVisibility(View.GONE);
        progress.setVisibility(View.GONE);

    }
}