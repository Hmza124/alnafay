package com.alnafay.app.screens;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.alnafay.app.popups.PlanSubmitPopup;
import com.bumptech.glide.Glide;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.alnafay.app.R;
import com.alnafay.app.adapters.AdapterForChildList;
import com.alnafay.app.fragments.ChildDetailFragment;
import com.alnafay.app.models.Profile;
import com.alnafay.app.models.ProfileChilds;
import com.alnafay.app.models.ProfileData;
import com.alnafay.app.network.APIPresenter;
import com.alnafay.app.network.APIResponse;
import com.alnafay.app.network.refreshResponse;
import com.alnafay.app.popups.ProfilePopup;
import com.alnafay.app.interfaces.onItemClick;
import com.alnafay.app.popups.ShareReferalCodePopup;
import com.alnafay.app.util.Library;
import com.rejowan.cutetoast.CuteToast;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;
import de.hdodenhof.circleimageview.CircleImageView;

public class DashBoardScreen extends AppCompatActivity implements View.OnClickListener, onItemClick, NavigationView.OnNavigationItemSelectedListener, APIResponse, refreshResponse {

    private RecyclerView recylerView;
    private RelativeLayout profileContainer, dashboardLL, headerL, container_1, container_2, container_3;
    private TextView norecordfound, status, totalMembers, totalPoints, userName, marqueeTxt, totalEarn, generateCodeBtn;
    private FrameLayout frame;
    private Library library;
    private ImageView refresh, verifiedIcon;
    private CircleImageView profileImage;
    private ImageView menu;
    private SharedPreferences sharedPreferences;
    private DrawerLayout drawerLayout;
    private NavigationView nav_view;
    private APIPresenter apiPresenter;
    private LinearLayout plansLL, totalAmountLL, refCodeBtn, indirect_members, direct_members;
    private int userID = -1;
    private Profile profile;
    public static int REQUEST_API = 0;
    private String totalAmount = "", withdrawalStatus = "";
    private boolean isWithdrawal = false;
    private ProgressBar progressImage;
    private ProfileData profileData;
    private LottieAnimationView animationView, arrowAnimationView;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_layout);

        recylerView = findViewById(R.id.recylerView);
        refCodeBtn = findViewById(R.id.refCodeBtn);
        norecordfound = findViewById(R.id.norecordfound);
        frame = findViewById(R.id.frame);
        status = findViewById(R.id.status);
        totalMembers = findViewById(R.id.totalMembers);
        totalPoints = findViewById(R.id.totalPoints);
        marqueeTxt = findViewById(R.id.marqueeTxt);
        profileImage = findViewById(R.id.profileImage);
        menu = findViewById(R.id.menu);
        profileContainer = findViewById(R.id.profileContainer);
        nav_view = findViewById(R.id.nav_view);
        refresh = findViewById(R.id.refresh);
        plansLL = findViewById(R.id.plansLL);
        dashboardLL = findViewById(R.id.dashboardLL);
        userName = findViewById(R.id.userName);
        totalEarn = findViewById(R.id.totalEarn);
        generateCodeBtn = findViewById(R.id.generateCodeBtn);
        headerL = findViewById(R.id.headerL);
        container_1 = findViewById(R.id.container_1);
        container_2 = findViewById(R.id.container_2);
        container_3 = findViewById(R.id.container_3);
        totalAmountLL = findViewById(R.id.totalAmountLL);
        animationView = findViewById(R.id.animationView);
        arrowAnimationView = findViewById(R.id.arrowAnimationView);
        indirect_members = findViewById(R.id.indirect_members);
        direct_members = findViewById(R.id.direct_members);

        verifiedIcon = findViewById(R.id.verifiedIcon);
        progressImage = findViewById(R.id.progressImage);

        apiPresenter = new APIPresenter(this, this);
        library = new Library(this);

        profileData = new ProfileData();

        nav_view.setNavigationItemSelectedListener(this);
        sharedPreferences = getSharedPreferences("status_pref", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        drawerLayout = findViewById(R.id.drawer_layout);

        userID = sharedPreferences.getInt("userID", -1);
        apiPresenter.userProfile(userID);
        library.showLoader();

        marqueeTxt.setSelected(true);

        recylerView.setLayoutManager(new GridLayoutManager(this, 4));

        refCodeBtn.setOnClickListener(this);
        menu.setOnClickListener(this);
        profileContainer.setOnClickListener(this);
        refresh.setOnClickListener(this);
        container_1.setOnClickListener(this);
        container_2.setOnClickListener(this);
        container_3.setOnClickListener(this);
        totalAmountLL.setOnClickListener(this);
        indirect_members.setOnClickListener(this);
        direct_members.setOnClickListener(this);

        getTilID();
    }

    private void getTilID() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("tilID");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                editor.putString("tilID", value);
                editor.apply();
            }

            @Override
            public void onCancelled(DatabaseError error) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == menu) {
            drawerLayout.openDrawer(Gravity.LEFT);
        } else if (v == profileContainer) {
            if (profile != null)
                ProfilePopup.showPopup(getSupportFragmentManager(), profile, this, this);
            else
                CuteToast.ct(this, "Please try again later.", CuteToast.LENGTH_LONG, CuteToast.ERROR, true).show();
        } else if (v == refresh) {
            apiPresenter.userProfile(userID);
            library.showLoader();
        } else if (v == refCodeBtn) {
            if (profileData != null)
                ShareReferalCodePopup.showPopup(getSupportFragmentManager(), profileData.getReferralCode());
        } else if (v == container_1) {
            openPlans("silver", container_1, "cardSilvertransaction");
        } else if (v == container_2) {
            openPlans("gold", container_2, "cardGoldtransaction");
        } else if (v == container_3) {
            openPlans("platinum", container_3, "cardPlatinumtransaction");
        } else if (v == totalAmountLL) {
            openWithdrawalScreen();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        animationView.playAnimation();
        if(REQUEST_API == 1) {
            REQUEST_API = 0;
            apiPresenter.userProfile(userID);
            library.showLoader();
        }
    }

    private void openPlans(String planName, View container, String name) {
        Intent intent = new Intent(this, GetPlanActiveScreen.class);

        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this,
                (View)container, name);
        intent.putExtra("plan_name", planName);
        intent.putExtra("profile", profile);
        startActivity(intent, options.toBundle());
    }

    @Override
    public void onClick(ProfileChilds data) {
        opentFragment(data, true);
    }

    public void opentFragment(ProfileChilds data, boolean isDirect) {
        ChildDetailFragment fragment = new ChildDetailFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putSerializable("childData", data);
        bundle.putBoolean("isDirect", isDirect);
        fragment.setArguments(bundle);
        transaction.addToBackStack("tag");
        transaction.add(R.id.frame, fragment).commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull @NotNull MenuItem item) {
        int viewId = item.getItemId();

        switch (viewId) {
            case R.id.withdraw:
                openWithdrawalScreen();
                break;
            case R.id.nav_share:
                shareCode();
                break;
            case R.id.packages:
                openPackage();
                break;
            case R.id.nav_logout:
                logoutRequest();
                break;
            case R.id.rewards:
                openRewards();
                break;
        }
        return false;
    }

    private void logoutRequest() {
        String email = sharedPreferences.getString("email", "");
        String password = sharedPreferences.getString("password", "");

        apiPresenter.loginUser(email, password, "");
        library.showLoader();
    }

    private void openRewards() {
        drawerLayout.closeDrawer(Gravity.LEFT);
        Intent intent = new Intent(this, RewardsScreen.class);
        startActivity(intent);
    }

    private void openPackage() {
        drawerLayout.closeDrawer(Gravity.LEFT);
        Intent intent = new Intent(this, PackagesDetailScreen.class);
        startActivity(intent);
    }

    private void openWithdrawalScreen() {
        if(profile!=null && profile.getIsActive()!=null) {
            if(profile.getIsActive().equals("active")) {
                Intent intent = new Intent(this, WithDrawalScreen.class);
                intent.putExtra("profile", profile);
                intent.putExtra("totalAmount", totalAmount);
                intent.putExtra("isWithdrawal", isWithdrawal);
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this,
                        (View)totalPoints, "totalPoints");
                startActivity(intent, options.toBundle());
                drawerLayout.closeDrawer(Gravity.LEFT);
            } else {
                drawerLayout.closeDrawer(Gravity.LEFT);
                CuteToast.ct(this, "Your account is not active yet.", CuteToast.LENGTH_LONG, CuteToast.WARN, true).show();
            }
        }
    }

    private void shareCode() {
        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        String shareBody = "You want to earn online. Download now & start earning with us.\n\n" + "https://play.google.com/store/apps/details?id=com.alnafay.app";
        intent.setType("text/plain");
        intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(intent, "Share application"));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onSuccess(String response, String type) {
        try {
            library.hideLoader();
            if(response!=null && !response.isEmpty() && type.equals("profile")) {
                JSONObject jsonObject = new JSONObject(response);
                profileData = new Gson().fromJson(jsonObject.get("payload").toString(), ProfileData.class);
                profile = profileData.getProfile();
                setProfileData(profile);
                totalPoints.setText((profileData.getTotalpoints()!=null && profileData.getTotalpoints()!=0) ? String.valueOf(profileData.getTotalpoints()) : "0");
                totalMembers.setText((profileData.getTotalMemebers()!=null && profileData.getTotalMemebers()!=0) ? String.valueOf(profileData.getTotalMemebers()) : "0");
                status.setText((profileData.getTotalchilds()!=null && profileData.getTotalchilds()!=0) ? String.valueOf(profileData.getTotalchilds()) : "0");
                Animation slide_down = AnimationUtils.loadAnimation(this, R.anim.slide_down);
                totalAmount = totalPoints.getText().toString();
                animationView.playAnimation();
                if(profile!=null && profile.getIsActive()!=null) {
                    if(profile.getIsActive().equals("active")) {

                        dashboardLL.setVisibility(View.VISIBLE);
                        plansLL.setVisibility(View.GONE);
                        if(!profileData.getWithdrawalStatus().equalsIgnoreCase("pending")) {
                            marqueeTxt.setText("Member");
                            verifiedIcon.setVisibility(View.VISIBLE);
                            isWithdrawal = false;
                        } else {
                            marqueeTxt.setText("Waiting for withdrawal");
                            verifiedIcon.setVisibility(View.GONE);
                            isWithdrawal = true;
                        }

                        headerL.setVisibility(View.VISIBLE);

                        nav_view.getHeaderView(0);
                        TextView nav_user = nav_view.findViewById(R.id.totalEarn);
                        nav_user.setText(profileData.getTotalearn()!=null && profileData.getTotalearn()!=0 ? String.valueOf(profileData.getTotalearn()) : "0");

                    } else if(profile.getIsActive().equals("pending")) {
                        verifiedIcon.setVisibility(View.GONE);
                        plansLL.setVisibility(View.VISIBLE);
                        dashboardLL.setVisibility(View.GONE);
                        marqueeTxt.setText("Processing...");
                        headerL.setVisibility(View.VISIBLE);

                    } else {
                        verifiedIcon.setVisibility(View.GONE);
                        plansLL.setVisibility(View.VISIBLE);
                        dashboardLL.setVisibility(View.GONE);
                        marqueeTxt.setText("Buy package to become member");
                        headerL.setVisibility(View.VISIBLE);
                    }
                    headerL.startAnimation(slide_down);
                }
            } else if(response!=null && type.equals("login")) {
                JSONObject object = new JSONObject(response);
                boolean status = object.getBoolean("status");
                if(status) {
                    logoutResponse();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            library.hideLoader();
        }
    }

    private void logoutResponse() {
        editor.putInt("userID", -1);
        editor.apply();
        Intent intent = new Intent(this, LoginScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void setProfileData(Profile profile) {
        if(profile!=null) {
            if(profile.getProfileImage()!=null && !profile.getProfileImage().isEmpty()) {
                Picasso.get().invalidate(profile.getProfileImage());
                progressImage.setVisibility(View.VISIBLE);
                Picasso.get().load(profile.getProfileImage()).placeholder(R.drawable.dummy_profile_image).into(profileImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        progressImage.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Exception e) {
                        progressImage.setVisibility(View.GONE);
                    }
                });
                CircleImageView image = nav_view.findViewById(R.id.profileImage);
                Picasso.get().load(profile.getProfileImage()).placeholder(R.drawable.dummy_profile_image).into(image);
            }
            TextView name = nav_view.findViewById(R.id.userName);
            name.setText(profile.getUsername()!=null ? profile.getUsername() : "");
            if(profile.getIsActive().equals("active")) {
                if(profile.getChildren()!=null && profile.getChildren().size() > 0) {
                    recylerView.setVisibility(View.VISIBLE);
                    arrowAnimationView.setVisibility(View.GONE);
                    recylerView.setAdapter(new AdapterForChildList(profile.getChildren(), this, this));
                    LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, R.anim.grid_layout_animation_from_bottom);
                    recylerView.setLayoutAnimation(animation);
                } else {
                    norecordfound.setVisibility(View.VISIBLE);
                    recylerView.setVisibility(View.GONE);
                    arrowAnimationView.setVisibility(View.VISIBLE);
                    arrowAnimationView.playAnimation();
                }
            }
        }
    }

    @Override
    public void onFailure(String message) {
        library.hideLoader();
    }

    @Override
    public void onRefresh() {
        if(REQUEST_API == 1) {
            REQUEST_API = 0;
            apiPresenter.userProfile(userID);
            library.showLoader();
        }
    }
}