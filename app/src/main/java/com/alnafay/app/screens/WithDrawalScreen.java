package com.alnafay.app.screens;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.alnafay.app.R;
import com.alnafay.app.models.Profile;
import com.alnafay.app.network.APIPresenter;
import com.alnafay.app.network.APIResponse;
import com.alnafay.app.popups.PlanSubmitPopup;
import com.alnafay.app.util.Library;
import com.rejowan.cutetoast.CuteToast;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

public class WithDrawalScreen extends AppCompatActivity implements View.OnClickListener, APIResponse {

    EditText amountEdt, userName, jazzCashNumber;
    TextView amoutText, totalAmount, pendingStatustext;
    Profile profile;
    String totalAmountValue;
    int amount = 0;
    Button submitBtn;
    APIPresenter apiPresenter;
    Library library;
    ImageView menu;
    boolean isWithdrawal = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.with_drawal_screen);

        amountEdt = findViewById(R.id.amountEdt);
        amoutText = findViewById(R.id.amoutText);
        totalAmount = findViewById(R.id.totalAmount);
        submitBtn = findViewById(R.id.submitBtn);
        userName = findViewById(R.id.userName);
        jazzCashNumber = findViewById(R.id.jazzCashNumber);
        menu = findViewById(R.id.menu);
        pendingStatustext = findViewById(R.id.pendingStatustext);

        apiPresenter = new APIPresenter(this, this);
        library = new Library(this);

        Intent intent = getIntent();
        if(intent!=null) {
            profile = (Profile) intent.getSerializableExtra("profile");
            totalAmountValue = intent.getStringExtra("totalAmount");
            isWithdrawal = intent.getBooleanExtra("isWithdrawal", false);
        }

        if(profile!=null && isWithdrawal) {
            submitBtn.setVisibility(View.GONE);
            pendingStatustext.setVisibility(View.VISIBLE);
        }

        submitBtn.setOnClickListener(this);

        amoutText.setText(totalAmountValue!=null && !totalAmountValue.isEmpty() ? totalAmountValue : "0");
        totalAmount.setText(totalAmountValue!=null && !totalAmountValue.isEmpty() ? totalAmountValue : "0");
        amount = Integer.parseInt(totalAmount.getText().toString());

        amountEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @SuppressLint("SetTextI18n")
            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().isEmpty()) {
                    int value = Integer.parseInt(s.toString());
                    if(value <= amount) {
                        int remaining = amount - value;
                        amoutText.setText("Your remaining balance will be " + String.valueOf(remaining));
                    } else{
                        amoutText.setText(totalAmountValue!=null && !totalAmountValue.isEmpty() ? totalAmountValue : "0");
                        CuteToast.ct(WithDrawalScreen.this, "Oh! you doesn't have this amount", CuteToast.LENGTH_LONG, CuteToast.WARN, true).show();
                    }
                } else {
                    amoutText.setText(totalAmountValue!=null && !totalAmountValue.isEmpty() ? totalAmountValue : "0");
                }
            }
        });

        menu.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v == submitBtn) {
            if(!isWithdrawal) {
                Library.hideKeyboard(this);

                if(amountEdt.getText().toString().isEmpty()) {
                    CuteToast.ct(this, getResources().getString(R.string.withdrawal_amount), CuteToast.LENGTH_LONG, CuteToast.ERROR, true).show();
                    return;
                }
                int amount = Integer.parseInt(amountEdt.getText().toString().trim());
                if(amount >= 100) {
                    if(userName.getText().toString().isEmpty()) {
                        CuteToast.ct(this, getResources().getString(R.string.your_name), CuteToast.LENGTH_LONG, CuteToast.ERROR, true).show();
                        return;
                    } else if(jazzCashNumber.getText().toString().isEmpty()) {
                        CuteToast.ct(this, getResources().getString(R.string.jazz_cash_number), CuteToast.LENGTH_LONG, CuteToast.ERROR, true).show();
                        return;
                    }

                    apiPresenter.requestWithdrawal(profile.getId(), userName.getText().toString().trim(), jazzCashNumber.getText().toString().trim(), amount);
                    library.showLoader();
                } else {
                    CuteToast.ct(this, getResources().getString(R.string.less_than_200), CuteToast.LENGTH_LONG, CuteToast.ERROR, true).show();
                }
            } else {
                CuteToast.ct(this, getResources().getString(R.string.pending), CuteToast.LENGTH_LONG, CuteToast.ERROR, true).show();
            }
        } else if(v == menu) {
            finish();
        }
    }

    @Override
    public void onSuccess(String response, String type) {
        library.hideLoader();
        try{
            if(response!=null) {
                boolean jsonObject = new JSONObject(response).getBoolean("status");
                if(jsonObject) {
                    PlanSubmitPopup.showPopup(getSupportFragmentManager(), profile, this, getString(R.string.receive_request_msg));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            CuteToast.ct(this, getResources().getString(R.string.try_again), CuteToast.LENGTH_LONG, CuteToast.ERROR, true).show();
        }
    }

    @Override
    public void onFailure(String message) {
        library.hideLoader();
        CuteToast.ct(this, getResources().getString(R.string.try_again), CuteToast.LENGTH_LONG, CuteToast.ERROR, true).show();
    }
}