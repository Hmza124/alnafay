package com.alnafay.app.screens;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.alnafay.app.R;

public class PackagesDetailScreen extends AppCompatActivity {

    ImageView menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.packages_detail_screen);

        menu = findViewById(R.id.menu);

        menu.setOnClickListener(v -> finish());
    }
}