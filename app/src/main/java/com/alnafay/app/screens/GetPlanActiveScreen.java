package com.alnafay.app.screens;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alnafay.app.R;
import com.alnafay.app.adapters.CustomPagerAdapter;
import com.alnafay.app.models.Profile;
import com.alnafay.app.network.APIPresenter;
import com.alnafay.app.network.APIResponse;
import com.alnafay.app.popups.PlanSubmitPopup;
import com.alnafay.app.util.Library;
import com.rejowan.cutetoast.CuteToast;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class GetPlanActiveScreen extends AppCompatActivity implements View.OnClickListener, APIResponse {

    private RelativeLayout container_1, container_2, container_3;
    private EditText amoutEdit, transactionID, tilIDEdit;
    private Button submitBtn;
    private Profile profile;
    private ViewPager viewpager;
    private ArrayList<Integer> arrayList;
    private LinearLayout layout_dot;
    private TextView[] dot;
    private APIPresenter apiPresenter;
    private int amount = 0;
    private Library library;
    private ImageView menu;
    private TextView pendingStatustext;
    private Runnable runnable;
    private Handler handler;
    private SharedPreferences preferences;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.get_plan_active_screen);

        container_1 = findViewById(R.id.container_1);
        container_2 = findViewById(R.id.container_2);
        container_3 = findViewById(R.id.container_3);
        amoutEdit = findViewById(R.id.amoutEdit);
        submitBtn = findViewById(R.id.submitBtn);
        viewpager = findViewById(R.id.viewpager);
        layout_dot = findViewById(R.id.layout_dot);
        transactionID = findViewById(R.id.transactionID);
        tilIDEdit = findViewById(R.id.tilIDEdit);

        menu = findViewById(R.id.menu);
        pendingStatustext = findViewById(R.id.pendingStatustext);

        apiPresenter = new APIPresenter(this, this);
        library = new Library(this);

        arrayList = new ArrayList<>();
        arrayList.add(R.drawable.banner_1_new);
        arrayList.add(R.drawable.banner_till_id_2);
        arrayList.add(R.drawable.banner_2_new);
        arrayList.add(R.drawable.banner_3_new);

        preferences = getSharedPreferences("status_pref", MODE_PRIVATE);
        String tilID = preferences.getString("tilID", "");

        tilIDEdit.setText(tilID!=null ? tilID : getString(R.string.message_later));

        Intent intent = getIntent();
        if(intent!=null) {
            String planName = intent.getStringExtra("plan_name");
            profile = (Profile) intent.getSerializableExtra("profile");

            switch (planName) {
                case "silver":
                    container_1.setVisibility(View.VISIBLE);
                    amoutEdit.setText("RS 1000");
                    amount = 1000;
                    break;
                case "gold":
                    container_2.setVisibility(View.VISIBLE);
                    amoutEdit.setText("RS 3000");
                    amount = 3000;
                    break;
                case "platinum":
                    container_3.setVisibility(View.VISIBLE);
                    amoutEdit.setText("RS 9000");
                    amount = 9000;
                    break;
            }
        }

        if(profile!=null && profile.getIsActive().equals("pending")) {
            submitBtn.setVisibility(View.GONE);
            pendingStatustext.setVisibility(View.VISIBLE);
        }

        CustomPagerAdapter pagerAdapter = new CustomPagerAdapter(getApplicationContext(), arrayList);
        viewpager.setAdapter(pagerAdapter);
        startAutoSlider(pagerAdapter.getCount());
        viewpager.setPageMargin(20);
        addDot(0);

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }
            @Override
            public void onPageSelected(int i) {
                addDot(i);
            }
            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        submitBtn.setOnClickListener(this);
        menu.setOnClickListener(this);
        tilIDEdit.setOnClickListener(this);
    }

    private void startAutoSlider(final int count) {
        handler = new Handler();
        runnable = () -> {
            int pos = viewpager.getCurrentItem();
            pos = pos + 1;
            if (pos >= count) pos = 0;
            viewpager.setCurrentItem(pos);
            handler.postDelayed(runnable, 4000);
        };
        handler.postDelayed(runnable, 4000);
    }

    @Override
    protected void onDestroy() {
        if(runnable!=null)
            handler.removeCallbacks(runnable);
        super.onDestroy();
    }

    public void addDot(int page_position) {
        dot = new TextView[arrayList.size()];
        layout_dot.removeAllViews();

        for (int i = 0; i < dot.length; i++) {
            dot[i] = new TextView(this);
            dot[i].setText(Html.fromHtml("&#9673;"));
            dot[i].setTextSize(35);
            dot[i].setTextColor(getResources().getColor(R.color.jet_black_2));
            layout_dot.addView(dot[i]);
        }
        //active dot
        dot[page_position].setTextColor(getResources().getColor(R.color.yellow_bilin));
    }

    @Override
    public void onClick(View v) {
        if(v == submitBtn) {
            if(profile!=null) {
                Library.hideKeyboard(this);

                if(transactionID.getText().toString().isEmpty()) {
                    CuteToast.ct(this, getResources().getString(R.string.put_id), CuteToast.LENGTH_LONG, CuteToast.ERROR, true).show();
                    return;
                }
                if(transactionID.getText().toString().trim().matches("[0-9]+") && transactionID.getText().toString().trim().length() > 2) {
                    library.showLoader();
                    apiPresenter.requestActive(profile.getId(), transactionID.getText().toString().trim(), amount);
                } else {
                    CuteToast.ct(this, getResources().getString(R.string.invalid), CuteToast.LENGTH_LONG, CuteToast.ERROR, true).show();
                }
            }
        } else if(v == menu) {
            finish();
        } else if(v == tilIDEdit) {
            library.copyValue(tilIDEdit.getText().toString().trim());
        }
    }

    @Override
    public void onSuccess(String response, String type) {
        library.hideLoader();
        try{
            if(response!=null) {
                boolean jsonObject = new JSONObject(response).getBoolean("status");
                if(jsonObject) {
                    PlanSubmitPopup.showPopup(getSupportFragmentManager(), profile, this, getString(R.string.active_request_msg));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            CuteToast.ct(this, getResources().getString(R.string.try_again), CuteToast.LENGTH_LONG, CuteToast.ERROR, true).show();
        }
    }

    @Override
    public void onFailure(String message) {
        library.hideLoader();
        CuteToast.ct(this, getResources().getString(R.string.try_again), CuteToast.LENGTH_LONG, CuteToast.ERROR, true).show();
    }
}