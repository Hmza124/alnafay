package com.alnafay.app.screens;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.alnafay.app.R;
import com.alnafay.app.network.APIPresenter;
import com.alnafay.app.network.APIResponse;
import com.alnafay.app.util.Constants;
import com.alnafay.app.util.Library;
import com.rejowan.cutetoast.CuteToast;

import org.json.JSONObject;

public class LoginScreen extends AppCompatActivity implements View.OnClickListener, APIResponse {

    Button signInBtn;
    TextView signUpBtn;
    EditText emailEditText, passwordEditText, refkeyEditText;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    ProgressBar progressBar;
    APIPresenter apiPresenter;
    Library library;
    String fcmToken = "", password = "", email = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);

        signUpBtn = findViewById(R.id.sign_up_tv);
        signInBtn = findViewById(R.id.signInBtn);
        emailEditText = findViewById(R.id.emailEditText);
        passwordEditText = findViewById(R.id.passwordEditText);
        refkeyEditText = findViewById(R.id.refkeyEditText);
        progressBar = findViewById(R.id.progressBar);

        apiPresenter = new APIPresenter(this, this);
        library = new Library(this);

        signUpBtn.setOnClickListener(this);
        signInBtn.setOnClickListener(this);

        sharedPreferences = getSharedPreferences("status_pref", MODE_PRIVATE);

        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        return;
                    }
                    fcmToken = task.getResult();
                });
    }

    private void registerUser() {
        Library.hideKeyboard(this);

        email = emailEditText.getText().toString().trim();
        password = passwordEditText.getText().toString().trim();

        emailEditText.setEnabled(false);
        passwordEditText.setEnabled(false);

        apiPresenter.loginUser(email, password, fcmToken);
    }

    private boolean validation() {
        if(emailEditText.getText().toString().isEmpty()) {
            emailEditText.setHintTextColor(Color.RED);
            emailEditText.setHint("Please enter email/phone");
            return false;
        } else if(passwordEditText.getText().toString().isEmpty()) {
            passwordEditText.setHintTextColor(Color.RED);
            passwordEditText.setHint("Please enter your password");
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        if (v == signUpBtn){
            Intent intent = new Intent(this, SignUpScreen.class);
            startActivity(intent);
        } else if (v == signInBtn) {
            if(validation()) {
                registerUser();
                progressBar.setVisibility(View.VISIBLE);
                signInBtn.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onSuccess(String response, String type) {
        try {
            if(response!=null && !response.isEmpty() && type.equals("login")) {
                CuteToast.ct(this, "Login successfully", CuteToast.LENGTH_LONG, CuteToast.SUCCESS, true).show();
                JSONObject object = new JSONObject(response);
                String userID = object.getJSONObject("payload").get("UserId").toString();
                editor = sharedPreferences.edit();
                editor.putInt("userID", Integer.parseInt(userID));
                editor.putString("email", email);
                editor.putString("password", password);
                editor.apply();
                Intent intent = new Intent(this, DashBoardScreen.class);
                startActivity(intent);
                finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
            progressBar.setVisibility(View.GONE);
            signInBtn.setVisibility(View.VISIBLE);
            emailEditText.setEnabled(true);
            passwordEditText.setEnabled(true);
            CuteToast.ct(this, "Please try again", CuteToast.LENGTH_LONG, CuteToast.ERROR, true).show();
        }
    }

    @Override
    public void onFailure(String message) {

        if(message!=null && !message.isEmpty()) {
            CuteToast.ct(this, message, CuteToast.LENGTH_LONG, CuteToast.ERROR, true).show();
        } else {
            CuteToast.ct(this, "Please try again", CuteToast.LENGTH_LONG, CuteToast.ERROR, true).show();
        }
        emailEditText.setEnabled(true);
        passwordEditText.setEnabled(true);
        progressBar.setVisibility(View.GONE);
        signInBtn.setVisibility(View.VISIBLE);
    }
}