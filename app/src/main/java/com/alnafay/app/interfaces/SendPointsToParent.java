package com.alnafay.app.interfaces;

import com.alnafay.app.models.UserDataModel;

public interface SendPointsToParent {
    void sendPointToParent (UserDataModel childData);
}
