package com.alnafay.app.network;

import com.google.gson.JsonElement;
import com.alnafay.app.models.RegisterResponse;

import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface {

    @POST("register")
    @FormUrlEncoded
    Call<JsonElement> registerUser(@Field("name") String name, @Field("email") String email, @Field("password") String password,
                                        @Field("password_confirmation") String password_confirmation, @Field("username") String username,
                                        @Field("phoneNumber") String phoneNumber, @Field("ref") String ref, @Field("imageUrl") String image, @Field("imageName") String imageName);

    @POST("login")
    @FormUrlEncoded
    Call<JsonElement> login(@Field("email") String email, @Field("password") String password, @Field("token") String token);

    @POST("profile")
    @FormUrlEncoded
    Call<JsonElement> userProfile(@Field("UserId") int email);

    @POST("getchilds")
    @FormUrlEncoded
    Call<JsonElement> getChilds(@Field("UserId") int userID);

    @POST("getcode")
    @FormUrlEncoded
    Call<JsonElement> getcode(@Field("UserId") int userID);

    @POST("approve-account-request")
    @FormUrlEncoded
    Call<JsonElement> requestActive(@Field("user_id") int userID, @Field("transaction_id") String transaction_id, @Field("amount") int amount);

    @POST("withdrawal-request")
    @FormUrlEncoded
    Call<JsonElement> requestWithdrawal(@Field("user_id") int userID, @Field("account_name") String name, @Field("account_number") String jazzcashNumber, @Field("amount") int amount);

    @POST("profile-image-update")
    Call<JsonElement> updateImage(@Body RequestBody request);
}