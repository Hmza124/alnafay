package com.alnafay.app.network;

public interface APIResponse {
    void onSuccess(String response, String type);
    void onFailure(String message);
}
