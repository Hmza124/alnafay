package com.alnafay.app.network;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonElement;
import com.alnafay.app.models.RegisterResponse;
import com.alnafay.app.util.Library;

import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class APIPresenter {

    APIResponse apiResponse;
    Context context;
    ApiInterface apiService;

    public APIPresenter(Context context, APIResponse apiResponse){
        this.context = context;
        this.apiResponse = apiResponse;
        apiService = ApiClient.getClient().create(ApiInterface.class);
    }

    public void registerUser(String name, String email, String password,
                             String password_confirmation, String username,
                             String phoneNumber, String ref, String image, String imageName) {
        Call<JsonElement> call = apiService.registerUser(name, email, password, password_confirmation, username, phoneNumber, ref, image, imageName);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement>call, Response<JsonElement> response) {
                try {
                    if(response!=null && response.isSuccessful()) {
                        JSONObject responseData = new JSONObject(response.body().toString());
                        if(responseData.getBoolean("status"))
                            apiResponse.onSuccess(responseData.getString("message"), "register");
                    } else {
                        String errorBody = response.errorBody().string();
                        JSONArray array = new JSONObject(errorBody.trim()).getJSONArray("payload");
                        String error = array.getString(0);
                        apiResponse.onFailure(error);
                    }
                    Log.d("register", call.request().toString());
                }catch (Exception e){
                    e.printStackTrace();
                    apiResponse.onFailure("");
                }
            }

            @Override
            public void onFailure(Call<JsonElement>call, Throwable t) {
                apiResponse.onFailure(t.getMessage());

            }
        });
    }

    public void loginUser(String email, String password, String token) {
        Call<JsonElement> call = apiService.login(email, password, token);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement>call, Response<JsonElement> response) {
                try {
                    if(response.isSuccessful()) {
                        apiResponse.onSuccess(response.body().toString(), "login");
                    } else {
                        String errorBody = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(errorBody.trim());
                        String error = jsonObject.getJSONObject("payload").get("error").toString();
                        apiResponse.onFailure(error);
                    }
                    Log.d("login", call.request().toString());
                }catch (Exception e){
                    e.printStackTrace();
                    apiResponse.onFailure("");
                }
            }

            @Override
            public void onFailure(Call<JsonElement>call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void userProfile(int userID) {
        Call<JsonElement> call = apiService.userProfile(userID);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement>call, Response<JsonElement> response) {
                try {
                    if(response!=null && response.isSuccessful()) {
                        apiResponse.onSuccess(response.body().toString(), "profile");
                    } else {
                        String errorBody = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(errorBody.trim());
                        String error = jsonObject.getJSONObject("payload").get("error").toString();
                        apiResponse.onFailure(error);
                    }
                    Log.d("login", call.request().toString());
                }catch (Exception e){
                    e.printStackTrace();
                    apiResponse.onFailure("");
                }
            }

            @Override
            public void onFailure(Call<JsonElement>call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void getChilds(int userID) {
        Call<JsonElement> call = apiService.getChilds(userID);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement>call, Response<JsonElement> response) {
                try {
                    if(response!=null && response.isSuccessful()) {
                        apiResponse.onSuccess(response.body().toString(), "child");
                    } else {
                        String errorBody = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(errorBody.trim());
                        String error = jsonObject.getJSONObject("payload").get("error").toString();
                        apiResponse.onFailure(error);
                    }
                    Log.d("login", call.request().toString());
                }catch (Exception e){
                    e.printStackTrace();
                    apiResponse.onFailure("");
                }
            }

            @Override
            public void onFailure(Call<JsonElement>call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void getcode(int userID) {
        Call<JsonElement> call = apiService.getcode(userID);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement>call, Response<JsonElement> response) {
                try {
                    if(response!=null && response.isSuccessful()) {
                        apiResponse.onSuccess(response.body().toString(), "getcode");
                    } else {
                        String errorBody = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(errorBody.trim());
                        String error = jsonObject.getJSONObject("payload").get("error").toString();
                        apiResponse.onFailure(error);
                    }
                    Log.d("login", call.request().toString());
                }catch (Exception e){
                    e.printStackTrace();
                    apiResponse.onFailure("");
                }
            }

            @Override
            public void onFailure(Call<JsonElement>call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void requestActive(int userID, String transactionID, int amout) {
        Call<JsonElement> call = apiService.requestActive(userID, transactionID, amout);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement>call, Response<JsonElement> response) {
                try {
                    if(response.body() !=null && response.isSuccessful()) {
                        apiResponse.onSuccess(response.body().toString(), "for_approve");
                    } else {
                        String errorBody = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(errorBody.trim());
                        String error = jsonObject.getJSONObject("payload").get("error").toString();
                        apiResponse.onFailure(error);
                    }
                    Log.d("requestActive", call.request().toString());
                }catch (Exception e){
                    e.printStackTrace();
                    apiResponse.onFailure("");
                }
            }

            @Override
            public void onFailure(Call<JsonElement>call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void requestWithdrawal(int userID, String name, String number, int amount) {
        Call<JsonElement> call = apiService.requestWithdrawal(userID, name, number, amount);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement>call, Response<JsonElement> response) {
                try {
                    if(response.body() !=null && response.isSuccessful()) {
                        apiResponse.onSuccess(response.body().toString(), "for_withdrawal");
                    } else {
                        String errorBody = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(errorBody.trim());
                        String error = jsonObject.getJSONObject("payload").get("error").toString();
                        apiResponse.onFailure(error);
                    }
                    Log.d("requestWithdrawal", call.request().toString());
                }catch (Exception e){
                    e.printStackTrace();
                    apiResponse.onFailure("");
                }
            }

            @Override
            public void onFailure(Call<JsonElement>call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void updateImage(RequestBody object) {
        Call<JsonElement> call = apiService.updateImage(object);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement>call, Response<JsonElement> response) {
                try {
                    if(response.body() !=null && response.isSuccessful()) {
                        apiResponse.onSuccess(response.body().toString(), "updateImage");
                    } else {
                        String errorBody = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(errorBody.trim());
                        String error = jsonObject.getJSONObject("payload").get("error").toString();
                        apiResponse.onFailure(error);
                    }
                    Log.d("updateImage", call.request().toString());
                }catch (Exception e){
                    e.printStackTrace();
                    apiResponse.onFailure("");
                }
            }

            @Override
            public void onFailure(Call<JsonElement>call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }
}