package com.alnafay.app.models;

import java.io.Serializable;
import java.util.ArrayList;

public class UserDataModel implements Serializable {

    private String name, email, phone, profileImage;
    private boolean isActive = false, isPointSend = false;
    private String totalPoint, sendParentPoints, pointsReceived;
    private String parentId, childId, referralId, pairID, userUId;
    private ArrayList<PairModel> pairJsonArray;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public void setReferralId(String referalId) {
        this.referralId = referalId;
    }

    public String getReferralId() {
        return referralId;
    }

    public String getChildId() {
        return childId;
    }

    public void setChildId(String childId) {
        this.childId = childId;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getTotalPoint() {
        return totalPoint;
    }

    public void setTotalPoint(String totalPoint) {
        this.totalPoint = totalPoint;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getUserUId() {
        return userUId;
    }

    public void setUserUId(String userUId) {
        this.userUId = userUId;
    }

    public boolean isPointSend() {
        return isPointSend;
    }

    public void setPointSend(boolean pointSend) {
        isPointSend = pointSend;
    }

    public String getSendParentPoints() {
        return sendParentPoints;
    }

    public void setSendParentPoints(String sendParentPoints) {
        this.sendParentPoints = sendParentPoints;
    }

    public String getPointsReceived() {
        return pointsReceived;
    }

    public void setPointsReceived(String pointsReceived) {
        this.pointsReceived = pointsReceived;
    }

    public ArrayList<PairModel> getPairJsonArray() {
        return pairJsonArray;
    }

    public void setPairJsonArray(ArrayList<PairModel> pairJsonArray) {
        this.pairJsonArray = pairJsonArray;
    }

    public String getPairID() {
        return pairID;
    }

    public void setPairID(String pairID) {
        this.pairID = pairID;
    }
}
