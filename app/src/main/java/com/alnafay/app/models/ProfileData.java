package com.alnafay.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProfileData implements Serializable {

    @SerializedName("profile")
    @Expose
    private Profile profile;
    @SerializedName("child")
    @Expose
    private Integer child;
    @SerializedName("TotalMemebers")
    @Expose
    private Integer totalMemebers;
    @SerializedName("totalpoints")
    @Expose
    private Integer totalpoints;
    @SerializedName("Totalchilds")
    @Expose
    private Integer Totalchilds;
    @SerializedName("totalearn")
    @Expose
    private Integer totalearn;
    @SerializedName("withdrawalStatus")
    @Expose
    private String withdrawalStatus;
    @SerializedName("referralCode")
    @Expose
    private String referralCode;

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Integer getChild() {
        return child;
    }

    public void setChild(Integer child) {
        this.child = child;
    }

    public Integer getTotalMemebers() {
        return totalMemebers;
    }

    public void setTotalMemebers(Integer totalMemebers) {
        this.totalMemebers = totalMemebers;
    }

    public Integer getTotalpoints() {
        return totalpoints;
    }

    public void setTotalpoints(Integer totalpoints) {
        this.totalpoints = totalpoints;
    }

    public Integer getTotalearn() {
        return totalearn;
    }

    public void setTotalearn(Integer totalearn) {
        this.totalearn = totalearn;
    }

    public Integer getTotalchilds() {
        return Totalchilds;
    }

    public void setTotalchilds(Integer totalchilds) {
        Totalchilds = totalchilds;
    }

    public String getWithdrawalStatus() {
        return withdrawalStatus!=null ? withdrawalStatus : "";
    }

    public void setWithdrawalStatus(String withdrawalStatus) {
        this.withdrawalStatus = withdrawalStatus;
    }

    public String getReferralCode() {
        return referralCode!=null ? referralCode : "";
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }
}
