package com.alnafay.app.models;

import java.io.Serializable;

public class PairModel implements Serializable {

    private String pairID, pairChildIds;
    private boolean isPointsSended;

    public String getPairID() {
        return pairID;
    }

    public void setPairID(String pairID) {
        this.pairID = pairID;
    }

    public String getPairChildIds() {
        return pairChildIds;
    }

    public void setPairChildIds(String pairChildIds) {
        this.pairChildIds = pairChildIds;
    }

    public boolean isPointsSended() {
        return isPointsSended;
    }

    public void setPointsSended(boolean pointsSended) {
        isPointsSended = pointsSended;
    }
}
